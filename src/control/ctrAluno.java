/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.sql.SQLException;
import model.Aluno;
import model.DAO.AlunoDAO;

/**
 *
 * @author aluno
 */
public class ctrAluno {
    
    public boolean insereAluno(int ra, String nome) throws SQLException, ClassNotFoundException{
        Aluno al = new Aluno(ra,nome);
        AlunoDAO alDAO = new AlunoDAO();
        boolean inseriu = alDAO.inserir(al);
        return inseriu;
    }

    public boolean excluiAluno(int exRa) throws SQLException, ClassNotFoundException{
        AlunoDAO alDAO = new AlunoDAO();
        boolean excluiu = alDAO.excluir(exRa);
        return excluiu;
    }
}
