/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.sql.Connection;
import model.Aluno;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import connection.Conexao;

/**
 *
 * @author aluno
 */
public class AlunoDAO {
    Connection con = null;
    
    public boolean inserir(Aluno al) throws SQLException, ClassNotFoundException{
        boolean inseriu = false;
        try{
            con = new Conexao().getConnection();
            String sql = "INSERT INTO Aluno (ra,nome) VALUES (?,?)";
            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setInt(1,al.getRa());
                stmt.setString(2,al.getNome());
                stmt.execute();
            }
            inseriu = true;
        }catch(SQLException ex){
        }
        finally{
            con.close();
        }
        return inseriu;
    }
    
    public boolean excluir(int exRa) throws ClassNotFoundException, SQLException{
        boolean excluiu = false;
        try{
            con = new Conexao().getConnection();
            String sql = "DELETE FROM Aluno WHERE ra = ?";
            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setInt(1,exRa);
                stmt.execute();
            }
            excluiu = true;
        }catch(ClassNotFoundException | SQLException ex){
            ex.printStackTrace();
        }
        finally{
            con.close();
        }
        return excluiu;
    }
}
